package com.sda.javarzw5.programowanie2.rest.model;

import java.util.Objects;

public class Kursanci {
    private int id;
    private String firstName;
    private String lastName;

    public Kursanci(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kursanci kursanci = (Kursanci) o;
        return id == kursanci.id &&
                Objects.equals(firstName, kursanci.firstName) &&
                Objects.equals(lastName, kursanci.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }

    @Override
    public String toString() {
        return "Kursanci{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
