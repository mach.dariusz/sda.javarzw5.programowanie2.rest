package com.sda.javarzw5.programowanie2.rest.controller;

import com.sda.javarzw5.programowanie2.rest.model.Kursanci;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*") // potrzebne ze Angular pozwolil na request
@RestController
@RequestMapping(path = "/api")
public class KursanciController {

    @GetMapping(path = "/kursanci")
    public List<Kursanci> getKursanci() {

        List<Kursanci> lista = new ArrayList<>();
        lista.add(new Kursanci(1, "Dariusz", "Mach"));
        lista.add(new Kursanci(2, "Katarzyna", "Faraś"));
        lista.add(new Kursanci(3, "Mirosław", "Malik"));
        lista.add(new Kursanci(4, "Paweł", "Bajda"));

        return lista;
    }

}
